<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master extends CI_Controller {

	public function __construct($config = 'rest') {
		header('Access-Control-Allow-Origin: *');
		header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
		header("Access-Control-Allow-Headers: *");

		parent::__construct();
		
		$this->load->model(array('authentication', 'data'));
		// $this->load->model('data');
	}

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index() {
		try {
			if ($this->input->server('REQUEST_METHOD') != 'OPTIONS') {
				$request = file_get_contents('php://input');
				$request = json_decode($request, true);
				$token = $this->input->get('token', TRUE);
				$username = $this->input->get('alias', TRUE);

				$auth = $this->authentication->tokenAuth($token, $username);
				if ($auth['verify']) {
					$data = $this->data->getData($request);

					if (isset($data) && $data && !isset($data['error']))
						$response = array('code' => 200, 'status' => 'success', 'message' => 'Pengambilan data berhasil', 'data' => $data);
					else if (isset($data) && $data) {
						if (isset($data['error']))
							$data = $data['error'];
						
						$response = array('code' => 200, 'status' => 'fail', 'message' => 'Pengambilan data gagal', 'data' => $data);
					} else
						$response = array('code' => 200, 'status' => 'fail', 'message' => 'Pengambilan data gagal', 'data' => array());

					echo json_encode($response);
				} else {
					$response = array('code' => 200, 'status' => 'fail_token', 'message' => 'Invalid token', 'error' => 'Ooops... Sepertinya akses anda berakhir... Mohon LogIn ulang aplikasi');
					echo json_encode($response);
				}
			} else {
				echo '';
			}
		} catch (Exception $e) {
			$response = array('code' => 200, 'status' => 'fail', 'message' => 'Pengambilan data gagal', 'error' => $e->getMessage());
			echo json_encode($response);
		}
	}

	public function delete() {
		try {
			if ($this->input->server('REQUEST_METHOD') != 'OPTIONS') {
				$request = file_get_contents('php://input');
				$request = json_decode($request, true);

				$token = $this->input->get('token', TRUE);
				$username = $this->input->get('alias', TRUE);

				$auth = $this->authentication->tokenAuth($token, $username);
				if ($auth['verify']) {
					$data = $this->data->deleteData($request);

					if (isset($data) && $data && !isset($data['error']))
						$response = array('code' => 200, 'status' => 'success', 'message' => 'Hapus data berhasil', 'data' => $data);
					else if ($data) {
						if (isset($data['error']))
							$data = $data['error'];
						
						$response = array('code' => 200, 'status' => 'fail', 'message' => 'Hapus data gagal', 'data' => $data);
					} else
						$response = array('code' => 200, 'status' => 'fail', 'message' => 'Hapus data gagal', 'data' => array());

					echo json_encode($response);
				} else {
					$response = array('code' => 200, 'status' => 'fail_token', 'message' => 'Invalid token', 'error' => 'Ooops... Sepertinya akses anda berakhir... Mohon LogIn ulang aplikasi');
					echo json_encode($response);
				}
			} else {
				echo '';
			}
		} catch (Exception $e) {
			$response = array('code' => 200, 'status' => 'fail', 'message' => 'Hapus data gagal', 'error' => $e->getMessage());
			echo json_encode($response);
		}
	}
}
