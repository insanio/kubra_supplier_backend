<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Android extends CI_Controller {

	public function __construct($config = 'rest') {
		header('Access-Control-Allow-Origin: *');
		header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
		header("Access-Control-Allow-Headers: *");

		parent::__construct();

		$this->load->model(array('authentication', 'custom'));
	}

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index() {
		echo 'Nya hahaha ~';
	}

	public function getuserdata() {
		try {
			if ($this->input->server('REQUEST_METHOD') != 'OPTIONS') {
				$request = file_get_contents('php://input');
				$request = json_decode($request, true);

				$token = $this->input->get('token', TRUE);
				$username = $this->input->get('alias', TRUE);

				$auth = $this->authentication->tokenAuth($token, $username);
				if ($auth['verify']) {
					$auth['token'] = $token;
				} else if (!$auth['verify'] && isset($auth['username']) && $auth['username']) {
					$auth = $this->authentication->tokenRefresh($auth['username']);
				}

				$token = isset($auth['token']) ? $auth['token'] : null;
				$username = isset($auth['alias']) ? $auth['alias'] : null;

				if ($token) {
					$data = $this->authentication->getUserData($request['username']);

					$token = isset($auth['token']) ? $auth['token'] : null;
					if ($data)
						$response = array('code' => 200, 'status' => 'success', 'message' => 'Pengambilan data user berhasil', 'data' => $data, 'token' => $token, 'alias' => $username);
					else
						$response = array('code' => 200, 'status' => 'fail', 'message' => 'Pengambilan data user gagal', 'data' => '', 'token' => $token, 'alias' => $username);

					echo json_encode($response);
				} else {
					$response = array('code' => 200, 'status' => 'fail_token', 'message' => 'Authentication fail', 'data' => '');

					echo json_encode($response);
				}
			} else {
				echo '';
			}
		} catch (Exception $e) {
			$response = array('code' => 200, 'status' => 'fail', 'message' => 'Pengambilan data gagal', 'error' => $e->getMessage());
			echo json_encode($response);
		}
	}

	public function getuserassetdata() {
		try {
			if ($this->input->server('REQUEST_METHOD') != 'OPTIONS') {
				$request = file_get_contents('php://input');
				$request = json_decode($request, true);

				$token = $this->input->get('token', TRUE);
				$username = $this->input->get('alias', TRUE);

				// $auth = $this->authentication->tokenAuth($token, $username);
				// if ($auth['verify']) {
				// 	$auth['token'] = $token;
				// } else if (!$auth['verify'] && isset($auth['username']) && $auth['username']) {
				// 	$auth = $this->authentication->tokenRefresh($auth['username']);
				// }

				// $token = isset($auth['token']) ? $auth['token'] : null;
				// $username = isset($auth['alias']) ? $auth['alias'] : null;

				// if ($token) {
				$data = $this->custom->getUserAssetData($request);

				$token = isset($auth['token']) ? $auth['token'] : null;
				if ($data)
					$response = array('code' => 200, 'status' => 'success', 'message' => 'Pengambilan data user berhasil', 'data' => $data, 'token' => $token, 'alias' => $username);
				else
					$response = array('code' => 200, 'status' => 'fail', 'message' => 'Pengambilan data user gagal', 'data' => '', 'token' => $token, 'alias' => $username);

				echo json_encode($response);
				// } else {
				// 	$response = array('code' => 200, 'status' => 'fail_token', 'message' => 'Authentication fail', 'data' => '');

				// 	echo json_encode($response);
				// }
			} else {
				echo '';
			}
		} catch (Exception $e) {
			$response = array('code' => 200, 'status' => 'fail', 'message' => 'Pengambilan data gagal', 'error' => $e->getMessage());
			echo json_encode($response);
		}
	}

	public function getassetlocation() {
		try {
			if ($this->input->server('REQUEST_METHOD') != 'OPTIONS') {
				$request = file_get_contents('php://input');
				$request = json_decode($request, true);
				$token = $this->input->get('token', TRUE);
				$username = $this->input->get('alias', TRUE);

				// $auth = $this->authentication->tokenAuth($token, $username);
				// if (!$auth['verify']) {
				// 	$auth = $this->authentication->tokenRefresh($username);
				// }

				// $token = isset($auth['token']) ? $auth['token'] : null;
				// $username = isset($auth['alias']) ? $auth['alias'] : null;

				$data = $this->custom->getAssetLocation($request);

				if ($data)
					$response = array('code' => 200, 'status' => 'success', 'message' => 'Fetch succeed', 'data' => $data, 'token' => $token, 'alias' => $username);
				else
					$response = array('code' => 200, 'status' => 'fail', 'message' => 'Fetch failed', 'data' => '', 'token' => $token, 'alias' => $username);

				echo json_encode($response);
			} else {
				echo '';
			}
		} catch (Exception $e) {
			$response = array('code' => 200, 'status' => 'fail', 'message' => 'Fetch failed', 'error' => $e->getMessage());
			echo json_encode($response);
		}
	}

	public function getbanner() {
		try {
			if ($this->input->server('REQUEST_METHOD') != 'OPTIONS') {
				$request = file_get_contents('php://input');
				$request = json_decode($request, true);

				$data = $this->custom->getBanner($request);

				if ($data)
					$response = array('code' => 200, 'status' => 'success', 'message' => 'Fetch succeed', 'data' => $data);
				else
					$response = array('code' => 200, 'status' => 'fail', 'message' => 'Fetch failed', 'data' => '');

				echo json_encode($response);
			} else {
				echo '';
			}
		} catch (Exception $e) {
			$response = array('code' => 200, 'status' => 'fail', 'message' => 'Fetch failed', 'error' => $e->getMessage());
			echo json_encode($response);
		}
	}
}
