<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	public function __construct($config = 'rest') {
		header('Access-Control-Allow-Origin: *');
		header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
		header("Access-Control-Allow-Headers: *");

		parent::__construct();

		$this->load->model(array('authentication'));
	}

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index() {
		try {
			if ($this->input->server('REQUEST_METHOD') != 'OPTIONS') {
				$request = file_get_contents('php://input');
				$request = json_decode($request, true);

				$data = $this->authentication->login($request['username'], $request['password']);

				if ($data)
					$response = array('code' => 200, 'status' => 'success', 'message' => 'Login berhasil', 'data' => $data);
				else
					$response = array('code' => 200, 'status' => 'fail', 'message' => 'Login gagal', 'data' => '');

				echo json_encode($response);
			} else {
				echo '';
			}
		} catch (Exception $e) {
			$response = array('code' => 200, 'status' => 'fail', 'message' => 'Pengambilan data gagal', 'error' => $e->getMessage());
			echo json_encode($response);
		}
	}

	public function register() {
		try {
			if ($this->input->server('REQUEST_METHOD') != 'OPTIONS') {
				$request = file_get_contents('php://input');
				$request = json_decode($request, true);

				$data = $this->authentication->register($request['username'], $request['password']);

				if ($data)
					$response = array('code' => 200, 'status' => 'success', 'message' => 'Registrasi berhasil', 'data' => $data);
				else
					$response = array('code' => 200, 'status' => 'fail', 'message' => 'Registrasi gagal', 'data' => '');

				echo json_encode($response);
			} else {
				echo '';
			}
		} catch (Exception $e) {
			$response = array('code' => 200, 'status' => 'fail', 'message' => 'Pengambilan data gagal', 'error' => $e->getMessage());
			echo json_encode($response);
		}
	}

	public function gettabledata() {
		try {
			if ($this->input->server('REQUEST_METHOD') != 'OPTIONS') {
				$request = file_get_contents('php://input');
				$request = json_decode($request, true);

				$token = $this->input->get('token', TRUE);
				$username = $this->input->get('alias', TRUE);

				$auth = $this->authentication->tokenAuth($token, $username);
				if ($auth['verify']) {
					$data = $this->authentication->getTableData($request['table']);

					$token = isset($auth['token']) ? $auth['token'] : null;
					if ($data)
						$response = array('code' => 200, 'status' => 'success', 'message' => 'Pengambilan data user berhasil', 'data' => $data, 'token' => $token);
					else
						$response = array('code' => 200, 'status' => 'fail', 'message' => 'Pengambilan data user gagal', 'data' => '', 'token' => $token);

					echo json_encode($response);

					// echo 'Success!';
				} else {
					$response = array('code' => 200, 'status' => 'fail_token', 'message' => 'Invalid token', 'error' => 'Ooops... Sepertinya akses anda berakhir... Mohon LogIn ulang aplikasi');
					echo json_encode($response);
				}
			} else {
				echo '';
			}
		} catch (Exception $e) {
			$response = array('code' => 200, 'status' => 'fail', 'message' => 'Pengambilan data gagal', 'error' => $e->getMessage());
			echo json_encode($response);
		}
	}

	public function getuserdata() {
		try {
			if ($this->input->server('REQUEST_METHOD') != 'OPTIONS') {
				$request = file_get_contents('php://input');
				$request = json_decode($request, true);

				$token = $this->input->get('token', TRUE);
				$username = $this->input->get('alias', TRUE);

				$auth = $this->authentication->tokenAuth($token, $username);
				if ($auth['verify']) {
					$data = $this->authentication->getUserData($request['username']);

					$token = isset($auth['token']) ? $auth['token'] : null;
					if ($data)
						$response = array('code' => 200, 'status' => 'success', 'message' => 'Pengambilan data user berhasil', 'data' => $data, 'token' => $token);
					else
						$response = array('code' => 200, 'status' => 'fail', 'message' => 'Pengambilan data user gagal', 'data' => '', 'token' => $token);

					echo json_encode($response);

					// echo 'Success!';
				} else {
					$response = array('code' => 200, 'status' => 'fail_token', 'message' => 'Invalid token', 'error' => 'Ooops... Sepertinya akses anda berakhir... Mohon LogIn ulang aplikasi');
					echo json_encode($response);
				}
			} else {
				echo '';
			}
		} catch (Exception $e) {
			$response = array('code' => 200, 'status' => 'fail', 'message' => 'Pengambilan data gagal', 'error' => $e->getMessage());
			echo json_encode($response);
		}
	}

	public function testfunction() {
		try {
			if ($this->input->server('REQUEST_METHOD') != 'OPTIONS') {
				$request = file_get_contents('php://input');
				$request = json_decode($request, true);

				// $data = $this->authentication->hashTest($request['username'], (isset($request['token']) ? $request['token'] : null));
				// $data = $this->authentication->tokenAuth((isset($request['token']) ? $request['token'] : null));
				$data = $this->authentication->register($request['username'], $request['password']);
				// $data = $this->config->item('table_data');

				echo json_encode($data);
			} else {
				echo '';
			}
		} catch (Exception $e) {
			$response = array('code' => 200, 'status' => 'fail', 'message' => 'Pengambilan data gagal', 'error' => $e->getMessage());
			echo json_encode($response);
		}
	}
}
