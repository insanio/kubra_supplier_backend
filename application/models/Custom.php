<?php
class Custom extends CI_Model {

    public function __construct() {
        parent::__construct();

        $this->load->database();
	}
	
	public function getAssetLocation($request) {
		// $sql = "SELECT * FROM santri WHERE nis = ? and cast(tgl_lahir as char(20)) = ? ";
		// $data = $this->db->query($sql, array($username, $password))->row();
		// return $data;

		// $return = $this->db->get_where('santri', array('nis' => $username, 'tgl_lahir' => $password));
		// if ($return) {
		// 	$return = $return->row();
		// } else {
		// 	$return = array();
		// }

		$sql = " SELECT ua.id as asset_id, ua.name as asset_name, ua.note, u.id as user_id, u.name as user_name, uac.phone, uac.mobile, uac.email, uac.address, uap_lat.alias as lat_alias, uap_lat.name as lat_name, cast(coalesce(uap_lat.value, '0') as double precision) as lat_value, uap_lng.alias as lng_alias, uap_lng.name as lng_name, cast(coalesce(uap_lng.value, '0') as double precision) as lng_value, uap_icon.alias as icon_alias, uap_icon.name as icon_name, uap_icon.value as icon_value FROM main.user_asset as ua inner join main.user as u on ua.user = u.id inner join main.user_asset_property as uap_lat on ua.id = uap_lat.user_asset and uap_lat.alias = 'map_latitude' and cast(coalesce(uap_lat.value, '0') as double precision) > ? and cast(coalesce(uap_lat.value, '0') as double precision) < ? inner join main.user_asset_property as uap_lng on ua.id = uap_lng.user_asset and uap_lng.alias = 'map_longitude' and cast(coalesce(uap_lng.value, '0') as double precision) > ? and cast(coalesce(uap_lng.value, '0') as double precision) < ? left join main.user_asset_property as uap_icon on ua.id = uap_icon.user_asset and uap_icon.alias = 'map_icon' left join main.user_asset_contact as uac on ua.id = uac.user_asset WHERE true ";
		$return = $this->db->query($sql, array($request['southwest']['latitude'], $request['northeast']['latitude'], $request['southwest']['longitude'], $request['northeast']['longitude']));

		if ($return) {
			$return = $return->result_array();
		} else {
			$return = array();
		}
		
		return array('data' => $return, 'total' => count($return));
	}
	
	public function getBanner($request) {
		$cond = "";
		$param = array();
		if (isset($request['date_start'])) {
			$cond = $cond . " and (ed.detail_value is not null and ed.detail_value < extract(epoch from timestamp ? )) ";
			array_push($param, $request['date_start']);
		}
		if (isset($request['date_end'])) {
			$cond = $cond . " and (ed.detail_value is not null and ed.detail_value > extract(epoch from timestamp ? )) ";
			array_push($param, $request['date_end']);
		}

		$sql = " select e.id, e.name, e.title, e.event, e.banner, ed.banner_time_start, ed.banner_time_end, ed.registration_time_start, ed.registration_time_end, ed.time_start, ed.time_end, ed.quota from internal.event as e left join internal.event_detail as ed on e.id = ed.event and ed.banner_time_start < now() and ed.banner_time_end > now() where true " . $cond;
		$return = $this->db->query($sql, $param);

		if ($return) {
			$return = $return->result_array();
		} else {
			$return = array();
		}
		
		return array('data' => $return, 'total' => count($return));
	}

	public function getUserAssetData($request) {
		$cond = "";
		$param = array();
		if (isset($request['id'])) {
			$cond = $cond . " and ua.id = ? ";
			array_push($param, $request['id']);
		}

		if (isset($request['parent_id'])) {
			$cond = $cond . " and ua.parent = ? ";
			array_push($param, $request['parent_id']);
		}

		if (isset($request['username'])) {
			$cond = $cond . " and u.username = ? ";
			array_push($param, $request['username']);
		}

		if (isset($request['asset_type'])) {
			$cond = $cond . " and ua.id in (select uat.user_asset from main.user_asset_type as uat inner join master.asset_type as at on at.id = uat.type and at.code = ?) ";
			array_push($param, $request['asset_type']);
		}

		$sql = " select ua.id, ua.user as user_id, u.username as user_username, ua.name, at.id as asset_type, at.name as asset_type_name, at.code as asset_type_code, uac.phone, uac.mobile, uac.email, uac.address, ua.note from main.user_asset as ua inner join main.user as u on ua.user = u.id left join main.user_asset_type as uat on ua.id = uat.user_asset left join master.asset_type as at on at.id = uat.type left join main.user_asset_contact as uac on ua.id = uac.user_asset where true " . $cond . " ";
		// echo "SQL: " . $sql;
		// print_r($request);
		$return = $this->db->query($sql, $param);

		if ($return) {
			$data = $return->result_array();

			if (count($data) > 0) {
				$subCond = $this->parseAssetId($data);

				$cond = $subCond['cond'] == "" ? "" : " and user_asset in ( " . $subCond['cond'] . " ) ";
				$sql = " select user_asset, alias, value from main.user_asset_property where true " . $cond . " ";
				$return = $this->db->query($sql, $subCond['param']);
				if ($return) {
					$property = $return->result_array();
					foreach($property as $value) {
						$subCond['data'][$value['user_asset']][$value['alias']] = $value['value'];
					}

					$data = array();
					foreach($subCond['data'] as $value) {
						array_push($data, $value);
					}

					$return = $data;
				} else {
					$return = array();
				}
			} else {
				$return = array();
			}
		} else {
			$return = array();
		}

		return array('data' => $return, 'total' => count($return));
	}

	private function parseAssetId($data) {
		$return = array();

		$cond = "";
		$param = array();
		foreach ($data as $asset) {
			if ($cond != "")
				$cond = $cond . ", ";
			$cond = $cond . " ? ";
			
			array_push($param, $asset['id']);

			$return[$asset['id']] = $asset;
		}

		return array('data' => $return, 'cond' => $cond, 'param' => $param);
	}
}

// SELECT ua.id as asset_id, ua.name as asset_name, ua.note, u.id as user_id, u.name as user_name, uac.phone, uac.mobile, uac.email, uac.address, uap_lat.alias as lat_alias, uap_lat.name as lat_name, cast(coalesce(uap_lat.value, '0') as double precision) as lat_value, uap_lng.alias as lng_alias, uap_lng.name as lng_name, cast(coalesce(uap_lng.value, '0') as double precision) as lng_value FROM main.user_asset as ua inner join main.user as u on ua.user = u.id left join main.user_asset_contact as uac on ua.id = uac.user_asset inner join main.user_asset_property as uap_lat on ua.id = uap_lat.user_asset and uap_lat.alias = 'map_latitude' and cast(coalesce(uap_lat.value, '0') as double precision) > -7.325338367048545 and cast(coalesce(uap_lat.value, '0') as double precision) < -7.306226571178492 inner join main.user_asset_property as uap_lng on ua.id = uap_lng.user_asset and uap_lng.alias = 'map_longitude' and cast(coalesce(uap_lng.value, '0') as double precision) > 112.74101592600347 and cast(coalesce(uap_lng.value, '0') as double precision) < 112.75646511465311 WHERE true

// {"southwest":{"longitude":"112.74101592600347","latitude":"-7.325338367048545"},"northeast":{"longitude":"112.75646511465311","latitude":"-7.306226571178492"}}

// select ua.id, ua.user as user_id, ua.name, uac.phone, uac.mobile, uac.email, uac.address, ua.note from main.user_asset as ua inner join main.user as u on ua.user = u.id left join main.user_asset_contact as uac on ua.id = uac.user_asset where true  and ua.parent = 2  and u.username = 'darkside'  and ua.id in (select uat.user_asset from main.user_asset_type as uat inner join master.asset_type as at on at.id = uat.type and at.code = 'product')