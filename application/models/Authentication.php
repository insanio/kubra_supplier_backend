<?php
class Authentication extends CI_Model {

    public function __construct() {
        parent::__construct();

        $this->load->database();
	}
	
	public function login($username, $password) {
		$token = null;
		$alias = null;

		/**
		 * Use this for postgres database type
		 */
		$sql = " SELECT l.username FROM application.login as l inner join application.login_detail as ld on l.username = ld.username WHERE l.username = ? and pgp_sym_decrypt(l.password::bytea, ld.salt) = ? ";

		/**
		 * Use this for mysql database type
		 */
		// $sql = " SELECT l.username FROM login as l inner join login_detail as ld on l.username = ld.username WHERE l.username = ? and sha1(concat(?, ld.salt)) = l.password ";
		$return = $this->db->query($sql, array($username, $password));
		if ($return && $return = $return->row()) {
			$token = $this->config->item('custom')['token'];

			$alias = bin2hex(random_bytes(10));
			$param = array(
				$token['columns']['token'] => $alias,
				$token['columns']['expire'] => $token['expire'],
				$token['columns']['regenerate'] => $token['regenerate'],
				$token['columns']['regeneration_time'] => time()
			);

			$primary = array(
				'username' => $username
			);

			$query = $this->db->update($token['schema_name'] . "." . $token['table_name'], $param, $primary);
			if ($query && !$this->db->affected_rows()) {
				$param['username'] = $username;
				$this->db->insert($token['schema_name'] . "." . $token['table_name'], $param);
			}

			$token = password_hash($alias, PASSWORD_BCRYPT, ['cost' => 10]);
		} else {
			return null;
		}

		return array('login' => $return, 'token' => $token, 'alias' => $alias);
	}
	
	public function register($username, $password) {
		$sql = " select gen_salt('bf', 10) as salt, 'Registered at backend' as description ";
		$crypt = $this->db->query($sql);
		if ($crypt) {
			$crypt = $crypt->row();
			
			$param = array(
				$username,
				$password,
				$crypt->salt,
				$crypt->description
			);
			$sql = " insert into application.login (username, password, description) values (?, pgp_sym_encrypt(?, ?), ?) ";
			if ($this->db->query($sql, $param)) {
				$param = array(
					$username,
					$crypt->description,
					$crypt->salt
				);

				$sql = " insert into application.login_detail (username, description, salt) values (?, ?, ?) ";
				if ($this->db->query($sql, $param)) {
					return array(
						"data" => array(
							'username' => $username,
							'description' => $crypt->description
						)
					);
				} else {
					$error = $this->db->error();
					if (isset($error['message']) && $error['message'] != '') {
						throw new Exception($error['message']);
					}
				}
			} else {
				$error = $this->db->error();
				if (isset($error['message']) && $error['message'] != '') {
					throw new Exception($error['message']);
				}
			}
		} else {
			throw new Exception('Salt generate process Fails');
		}
	}

	public function getTableData($table) {
		$return = array();

		if ($tableData = $this->config->item('table_data')) {
			$return['data'] = $tableData[$table];
			$return['table_source'] = 'local_variable';
		} else {
			$sql = ' select id, parent as table_property_parent, code, name, value from application.table_property where "table" = ? order by property_value asc ';
			$data = $this->db->query($sql, array($table));
			if ($data) {
				$tableProperty = array();
				foreach ($data->result_array() as $value) {
					$tableProperty[$value['id']] = $value;
				}

				$sql = " select table_property, code, name, value from application.table_detail where table_property in (select id from application.table_property where \"table\" = ?) order by detail_value asc ";
				$data = $this->db->query($sql, array($table));
				if ($data) {
					$tableDetail = $data->result_array();

					foreach ($tableDetail as $value) {
						if (isset($tableProperty[$value['table_property']])) {
							if (!isset($tableProperty[$value['table_property']]['value']))
								$tableProperty[$value['table_property']]['value'] = array();
							// $tableProperty[$value['table_property']] = array();

							if (is_array($tableProperty[$value['table_property']]['value']))
								array_push($tableProperty[$value['table_property']]['value'], $value);
						}
					}
				}

				$data = array();
				foreach ($tableProperty as $value) {
					// unset($value['id']);
					array_push($data, $value);
				}

				$return['data'] = $data;
				$return['table_source'] = 'database';
			}
		}

		return $return;
	}

	public function getUserData($username) {
		$return = array();

		$sql = " select u.id, u.name, uc.phone, uc.mobile, uc.email, uc.address from main.user as u left join main.user_contact as uc on u.id = uc.user where u.username = ? ";
		// $sql = " select * from santri where nis = ? ";
		$data = $this->db->query($sql, array($username));
		if ($data) {
			$user = $data->row_array();

			$sql = " select id, parent, name, alias, value from main.user_property where user = ? ";
			$data = $this->db->query($sql, array($user['id']));
			if ($data) {
				$userProperty = array();
				foreach ($data->result_array() as $value) {
					$userProperty[$value['id']] = $value;
				}

				$user['property'] = $userProperty;
			}

			$return['user'] = $user;
		}

		if ($menuData = $this->config->item('menu_data')) {
			$return['menu'] = $menuData;
		} else {
			$sql = " select m.id, m.parent as menu_parent, m.module, m.application, m.table, m.name, m.menu_description, m.title, m.link, m.url, m.icon, m.group, m.expanded from application.menu as m left join application.access as a on a.username = ? and a.module = m.module and a.application = m.application where true order by m.menu_value ";
			// $sql = ' select id, parent as menu_parent, module, application, "table", name, menu_description, title, link, icon from application.menu ';
			$data = $this->db->query($sql, array($username));
			if ($data) {
				$menu = array();
				foreach ($data->result_array() as $value) {
					$menu[$value['id']] = $value;
				}

				$sql = " select id, menu, code, name, value from application.menu_property ";
				$data = $this->db->query($sql);
				if ($data) {
					$menuProperty = $data->result_array();

					foreach ($menuProperty as $value) {
						if (isset($menu[$value['menu']])) {
							if (!isset($menu[$value['menu']]['property']))
								$menu[$value['menu']]['property'] = array();

							$menu[$value['menu']]['property'][$value['id']] = $value;
						}
					}
				}

				$data = array();
				foreach ($menu as $value) {
					array_push($data, $value);
				}

				$return['menu'] = $data;
			}
		}

		// if ($menuData = $this->config->item('menu_data')) {
		// 	$return['menu'] = $menuData;
		// }

		return $return;
	}

	public function tokenAuth($token, $username) {
		$tokenConfig = $this->config->item('custom')['token'];
		$alias = $this->db->get_where($tokenConfig['schema_name'] . "." . $tokenConfig['table_name'], array('current_token' => $username))->row();
		if ($alias) {
			$auth = array('verify' => password_verify($alias->current_token, $token), 'username' => $alias->username);

			$time = time();
			if ($time - $alias->current_token_regeneration_time < $alias->current_token_expire && $auth['verify']) {
				if ($time - $alias->current_token_regeneration_time > $alias->current_token_regenerate) {
					$auth['token'] = password_hash($alias->current_token, PASSWORD_BCRYPT, ['cost' => 10]);
				}

				$this->db->update($tokenConfig['schema_name'] . "." . $tokenConfig['table_name'], 
				array(
					$tokenConfig['columns']['regeneration_time'] => $time
				), array(
					'current_token' => $username
				));
			} else {
				$auth['verify'] = false;

				$this->db->update($tokenConfig['schema_name'] . "." . $tokenConfig['table_name'], 
				array(
					$tokenConfig['columns']['token'] => null,
					$tokenConfig['columns']['expire'] => null,
					$tokenConfig['columns']['regenerate'] => null,
					$tokenConfig['columns']['regeneration_time'] => null
				), array(
					'current_token' => $username
				));
			}
		} else {
			$auth = array('verify' => false);
		}

		return $auth;
	}

	public function tokenRefresh($username) {
		$token = $this->config->item('custom')['token'];

		$alias = bin2hex(random_bytes(10));
		$param = array(
			$token['columns']['token'] => $alias,
			$token['columns']['expire'] => $token['expire'],
			$token['columns']['regenerate'] => $token['regenerate'],
			$token['columns']['regeneration_time'] => time()
		);

		$primary = array(
			'username' => $username
		);

		$query = $this->db->update($token['schema_name'] . "." . $token['table_name'], $param, $primary);
		if ($query && !$this->db->affected_rows()) {
			$param['username'] = $username;
			$this->db->insert($token['schema_name'] . "." . $token['table_name'], $param);
		}

		$token = password_hash($alias, PASSWORD_BCRYPT, ['cost' => 10]);

		return array('token' => $token, 'alias' => $alias);
	}

	// public function hashTest($username, $token) {
	// 	$options = [
	// 		'cost' => 10
	// 	];

	// 	$hash = password_hash($username, PASSWORD_BCRYPT, $options);

	// 	echo $hash . '<br/>';

	// 	$unhash = password_verify($username, $hash);

	// 	echo $unhash;
	// }
}
