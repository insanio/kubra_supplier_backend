<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if (!function_exists('escape_columnlist')) {
    function escape_columnlist($column) {
		if ($column == 'user')
            $column = '"' . $column . '"';
            
        if ($type == 'datetime-local')
            $column = $column . '::timestamp';
            
        if ($type == 'date')
            $column = $column . '::date';

        return $column;
    }   
}

if (!function_exists('check_valuetype')) {
    function check_valuetype($val, $field) {
        // echo 'Check value: ' . $val . ', ';
        // print_r($field);

        switch ($field->type) {
            case 'int':
            case 'int2':
            case 'int4':
            case 'int8':
            settype($val, 'integer');
            break;

            case 'float8':
            case 'decimal':
            settype($val, 'float');
            break;

            case 'bool':
            settype($val, 'boolean');
            break;
        }
        // echo 'After Check value: ' . $val . ', ';
        
        return $val;
    }
}

if (!function_exists('replace_error')) {
    function replace_error($error) {
        $ret = "";
        if (strrpos($error, "referen")) {
            $ret = "Data ini masih direferensikan oleh data lain, Mohon hapus data lain terlebih dahulu!";
        } else if (strrpos($error, "dupli")) {
            $ret = "Data dengan kode ini sudah ada pada database, Mohon gunakan kode lain!";
        } else {
            $ret = $error;
        }

        return $ret;
    }
}